{-
Goal
====
As outlined in README.md, we want to implement the DataMemberOf type class that will allow us to *lift* a
transformation on a data member into a transformation on the entire type:

  liftMemberTrans :: (a `DataMemberOf` b) => (a -> a) -> (b -> b)



Type class requirements
=======================
- First, let's get the necessary language extensions and imports out of the way (don't pay too much attention
  to them yet):
-}

{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Trans(
    Trans
  , liftMemberTrans
) where

import GHC.Generics( Generic(..), K1(..), M1(..), Rec0, type (:*:)(..) )
import Data.Kind( Type )
import Data.Type.Bool( type (||), Not )

import Flow( (.>) )

{-
- Next, a type synonym that will come in handy:
-}

type Trans a = a -> a


{-
- And now, let the real work begin! Recall the type signature of liftMemberTrans:

    liftMemberTrans :: (a `DataMemberOf` b) => (a -> a) -> (b -> b)

  This means that the DataMemberOf type class must meet the following requirements:

  - R1: It must represent faithfully the "has a data member of this type" relationship. E.g., if
    
      data S = S Int Bool

    then the (Int `DataMemberOf` S) and (Bool `DataMemberOf` S) instances must be derived automatically by GHC,
    but for any type T that is neither Int nor Bool, the (T `DataMemberOf` S) instance __must not__ be derived
    automatically.

  - R2: Its methods must be sufficient for implementing liftMemberTrans.


- Let's focus on the second requirement: imagine that an (a `DataMemberOf` b) instance exists and we have
  f :: a -> a. What are the concrete steps we have to take in order to lift f into a transformation on b? 
    1. *Extract* the value of the 'a' data member within 'b'.
    2. *Apply* f to that value, thereby obtaining a new 'a' value.
    3. *Update* the 'a' data member in 'b' with this new value and leave the rest of 'b' as is.

  Obviously, we don't need any help from DataMemberOf for step 2, but we do need it for steps 1 and 3. So, the
  type class should look something like this:

    class a `DataMemberOf` b where
      extractMember :: b -> a
      updateMember :: a -> b -> b

  Using that, we can indeed define liftMemberTrans:

    liftMemberTrans :: (a `DataMemberOf` b) => Trans a -> Trans b
    (liftMemberTrans f) b = updateMember (f (extractMember b)) b 


- Before we jump into implementation, let's consider one last thing. Assume that we have

    data W = W Int Bool Int

  and f :: Int -> Int; how do we lift f to W? Which Int data member of W should we apply f to? 

  More generally: if 'b' has multiple data members of type 'a', and we're lifting 'f :: a -> a' to 'b', then
  which data member of 'b' should we apply f to?

  Recall that we want to implement DataMemberOf in a __generic__ way, therefore we need a __general, programmatic__
  method of deciding what to do in such situations. The following potential solutions come to mind:

    1. Apply f to the first data member of type 'a'.
       - E.g., if w = W 1 True 2, then (liftMemberTrans f) w would be W (f 1) True 2.

    2. Apply f to all data members of type 'a'.
       - E.g., if w = W 1 True 2, then (liftMemberTrans f) w would be W (f 1) True (f 2).

    3. Don't allow lifting of transformations if 'b' has multipe data members of type 'a'.
       - E.g., with W above, liftMemberTrans f :: W -> W should not compile.


- So, which one should we pick? Let's take into account the following:

  - In the original use case for liftMemberTrans (see README.md), we need to lift f :: S -> S to ES, where

      data ES = ES{ s :: S, newField :: ... }

    and S is a record containing several fields already, whereas newField is an extra field we need to keep
    track of. In those circumstances it's practically impossible for s and newField to have the same type;
    in other words, we'd never encounter the "multiple data members of the same type" problem in the typical
    use case.

  - If type 'b' has several data members of type 'a', then it's likely that those members represent different
    *concepts*: e.g., consider

      data SumOfNaturals = SumOfNaturals{ numberOfSummands :: Natural, sum :: Natural }

    To the human reader it's obvious that the two Naturals serve different purposes - but GHC doesn't
    understand that!

    Therefore, in general, if we have an arbitrary f :: a -> a transformation, it's very unlikely that either
    option 1 or option 2 above would do "the right thing" in the majority (or even a sizable minority) of the
    cases that occur in practice.
    

- Based on these considerations, I decided to go for option 3, i.e., only define the (a `DataMemberOf` b) instance
  automatically if 'b' has a unique data member of type 'a'. And to better express that restriction, I slightly
  changed the type class name:
-}

class member `UniqueMemberOf` whole where
  extractMember :: whole -> member
  updateMember :: member -> whole -> whole

{-
- Using that, we can define liftMemberTrans easily:
-}

liftMemberTrans :: (member `UniqueMemberOf` whole) => Trans member -> Trans whole
liftMemberTrans f = dup $ extractMember .> f .> updateMember

{-
  Notes:
  - I used Flow's '.>' forward composition operator to better express the fact that lifting consists of the
    "extract - apply - update" steps described earlier.

  - 'dup' (also known as the W combinator) is a useful general utility function:
-}

dup :: (a -> a -> b) -> (a -> b)
dup f x = f x x 

{-
    We need it here because 'liftMemberTrans f' uses 'whole' twice: first to extract 'member' from it, and
    then to update 'member' in it with the transformed value.



Automatic creation of instances
===============================
- We've got the UniqueMemberOf methods fleshed out - but how can we ensure that the necessary instances are
  created by GHC __automatically__ ? E.g., for the type mentioned earlier:

    data S = S Int Bool

  we need the (Int `UniqueMemberOf` S) and (Bool `UniqueMemberOf` S) instances to be created by GHC - how can
  we do that?


- Well, if you have ever used aeson, for example, then you'll recall what's happening there is similar to what we
  need here. More precisely, if you use aeson and want a ToJSON instance for your type, all you need to do is this:

    {-# LANGUAGE DeriveAnyClass #-}
    {-# LANGUAGE DeriveGeneric #-}

    import GHC.Generics( Generic )
    import Data.Aeson( ToJSON )

    data MyType = MyType{ ... fields ... }
      deriving (Generic, ToJSON)


- What we need is slightly different: we want automatic instances of a __two-parameter__ type class for certain
  pairs of types (without knowing the actual concrete types in the pair), whereas aeson gives us an automatic
  instance of a single-parameter type class for a concrete type. Still, using DeriveGeneric and the Generic type
  class looks like a promising way to go, so let's read up on them here:
    
    https://hackage.haskell.org/package/base-4.16.1.0/docs/GHC-Generics.html !


- The essence of what we learn there is that if you derive automatically an instance of Generic for your type T,
  GHC provides you with the following:
  - A representation type ('Rep T'), which has a __standard structure__ and is isomorphic to T.
    - "Standard structure" means that the representation type is built up by using a finite set of known type
      constructors, all defined in GHC.Generics.
  - A function ('from') that maps a value of your type to a value of the representation type.
  - A function ('to') that performs the mapping in the other direction.


- So, if the (Generic b) instance exists, we have a strategy for the automatic creation of (a `UniqueMemberOf` b)
  instances: do everything at the level of the __representation type__! 

  More precisely:
  - Express the concept corresponding to (a `UniqueMemberOf` b), i.e., the "b has a unique data member of type a" 
    relationship, in terms of 'a' and 'Rep b'.
  - Implement the methods of UniqueMemberOf in terms of 'from', 'to', and functions operating on 'a' and 'Rep b'
    values.



Moving to the representation level
==================================
- But how can we actually follow that strategy, i.e., transport the whole problem to the representation type?

  First of all, let's get more familiar with the structure of the latter: e.g., if we have

    data T = T Int Char String Double
      deriving Generic

  then 'Rep T' looks like this:

    D1
      (...)
      (
        C1
          (...)
          (
            (
              S1
                (...)
                (Rec0 Int)
              :*:
              S1
                (...)
                (Rec0 Char)
            )
            :*:
            (
              S1
                (...)
                (Rec0 String)
              :*:
              S1
                (...)
                (Rec0 Double)
            )
          )
      )

    ...where D1, C1, S1, :*: and Rec0 are type constructors defined in GHC.Generics.


- Let's look at that structure in more detail:
  - D1 is its root; the (...) part contains metadata about the type T (e.g., which module it's defined in).

  - The children of D1 are C1's, one for each data constructor of T
    - Every C1 contains metadata about the corresponding constructor (e.g., its name).

  - Below a C1 we find S1's, one for each field (or "data member") that appears in the constructor
    - Every S1 contains metadata about the corresponding field (e.g., its name if T uses record syntax).
    - Precisely speaking, the S1's are not directly under C1: rather, they are assembled into a *binary tree*
      via the :*: operator, and C1 contains the root of that tree.

  - Under every S1, a 'Rec0 t' entry expresses the fact that the corresponding field's type is t.


  Generalizing from this example, we can conclude the following:

    If 'b' is a plain old record type like T above, and the (Generic b) instance exists, then 'a' is the type of
    a data member in 'b' if and only if 'Rep b' has a 'Rec0 a' entry.


- This latter observation means that in order to represent faithfully the "b has a unique data member of type a"
  relationship, it's enough for us to take 'Rep b' and look for 'Rec0 a' entries in it. More precisely, the idea
  is the following:

  1. Define

      class a `UniqueMemberOfRep` rep
    
    to represent the notion that a unique 'Rec0 a' entry exists in 'rep'.


  2. Derive a UniqueMemberOfRep instance for every type constructor that's used in building up the 'Rep' structure:
     
      instance a `UniqueMemberOfRep` Rec0 a where ...

      instance (a `UniqueMemberOfRep` rep) => a `UniqueMemberOfRep` S1 ... rep where ...

      ...

      instance (...) => a `UniqueMemberOfRep` left :*: right where ...


- So, UniqueMemberOfRep helps us take care of requirement R1 postulated earlier: we simply say that

    instance (Generic b, a `UniqueMemberOfRep` Rep b) => a `UniqueMemberOf` b where ...

  But what about the other requirement, R2, that concerns the methods in UniqueMemberOf? Can UniqueMemberOfRep
  help us with those too? Recall that we need the following:

    class a `UniqueMemberOf` b where
      extractMember :: b -> a
      updateMember :: a -> b -> b

  If we assume that the (Generic b) instance exists, can that help us define those methods?


- The answer is, unsurprisingly, yes :) Recall that GHC provides us with conversion functions between b and its
  representation type:

    from :: b -> Rep b x
    to :: Rep b x -> b

  (Note: the 'x' type argument is just a technicality, you can safely ignore it.)

  Going back to the example above, if
  
    t = T 1 'a' "Hello" 3.14
    
  then the 'from t' value has the same basic tree structure as 'Rep T' above, with the appropriate value
  constructors taking the place of type constructors D1, C1, S1, etc.
  
  This means that we can __recover the values__ of t's data members from it: e.g. the value 1 is sitting there
  in 'from t' as the argument to the value constructor that corresponds to the 'Rec0 Int' entry in Rep T.
  And vice versa, we can __change__ the value of a data member and create a new T value: e.g., if we take 'from t',
  change the argument to the 'Rec0 Int' value constructor from 1 to 2, and call 'to' on the result, then we get
  T 2 'a' "Hello" 3.14.


- So, what we need in general is the capability to:
  - Extract from a 'Rep b x' the argument to the value constructor that corresponds to the 'Rec0 a' entry
    in 'Rep b'.
  - Take a 'Rep b x' and change in it the argument to the value constructor that corresponds to the 'Rec0 a'
    entry in 'Rep b', thereby obtaining a new 'Rep b x' value.

  In other words, we need the following methods in UniqueMemberOfRep:

    class a `UniqueMemberOfRep` rep where
      repExtractMember :: rep k -> a
      repUpdateMember :: a -> rep k -> rep k

  (Just like before, the 'k' type parameter is a technicality - ignore it.)

  Note: although signatures of the methods don't mention 'Rec0', their implementations in the actual
  UniqueMemberOfRep instances will ensure that they will be doing what we outlined above, i.e., extract or
  update values corresponding to Rec0 entries.


- Now we know enough to define UniqueMemberOfRep and use it to implement the methods of UniqueMemberOf:
-}

class member `UniqueMemberOfRep` repWhole where
  repExtractMember :: repWhole k -> member
  repUpdateMember :: member -> repWhole k -> repWhole k


instance (Generic whole, member `UniqueMemberOfRep` Rep whole) => member `UniqueMemberOf` whole where
  extractMember = from .> repExtractMember
  updateMember m = from .> repUpdateMember m .> to

{-

Defining instances of UniqueMemberOfRep
=======================================
- The only thing that's missing now is the actual definition of UniqueMemberOfRep instances as outlined
  earlier, i.e., start with

    instance a `UniqueMemberOfRep` Rec0 a 

  and continue with additional definitions corresponding to the type constructors that occur in 'Rep'
  (S1, C1, etc).


- Let's get started then: first, Rec0!
-}

instance a `UniqueMemberOfRep` Rec0 a where
  repExtractMember (K1 a) = a 
  repUpdateMember a (K1 _) = K1 a

{-
  (Note: 'K1' is the value constructor corresponding to 'Rec0'.)


- That was easy - now on to S1, D1 and C1!

  It turns out that all three are specializations of a more general type constructor, M1, which means that
  one instance definition will suffice:
-}

--
-- Metadata: D1 (datatype declaration) / C1 (constructor) / S1 (member selector).
--
instance (a `UniqueMemberOfRep` rep) => a `UniqueMemberOfRep` M1 tag meta rep where
  repExtractMember (M1 rep_k) = repExtractMember rep_k
  repUpdateMember a (M1 rep_k) = M1 $ repUpdateMember a rep_k


{-
- We're almost done: only ':*:' remains - let's deal with that too! 

  - We quickly realize that some extra work is needed:
    - If the (a `UniqueMemberOfRep` left) instance exists, then the (a `UniqueMemberOfRep` left :*: right)
      instance should exist only if 'a' does not occur __at all__ in a Rec0 entry within 'right'. Otherwise,
      'a' would no longer occur in a unique Rec0 entry within 'left :*: right'.

    - Likewise, if the (a `UniqueMemberOfRep` right) instance exists, then 'a' must not occur in 'left' at all
      for the (a `UniqueMemberOfRep` left :*: right) instance to exist.

  - Let's capture the "does not occur in a Rec0 entry" requirement via a type class and an auxiliary type family:
-}

type a `NotMemberOfRep` rep = a `IsMemberOfRep` rep ~ 'False

type family a `IsMemberOfRep` rep where
  a `IsMemberOfRep` (Rec0 a) = 'True
  a `IsMemberOfRep` (M1 tag metadata rep) = a `IsMemberOfRep` rep
  a `IsMemberOfRep` left :*: right = a `IsMemberOfRep` left || a `IsMemberOfRep` right
  _ `IsMemberOfRep` _ = 'False

{-
  - We can now define the instances corresponding to ':*:' :

instance (a `UniqueMemberOfRep` left, a `NotMemberOfRep` right) => a `UniqueMemberOfRep` (left :*: right) where
  repExtractMember (left_k :*: _) = repExtractMember left_k
  repUpdateMember a (left_k :*: right_k) = repUpdateMember a left_k :*: right_k 


instance (a `NotMemberOfRep` left, a `UniqueMemberOfRep` right) => a `UniqueMemberOfRep` (left :*: right) where
  repExtractMember (_ :*: right_k) = repExtractMember right_k
  repUpdateMember a (left_k :*: right_k) = left_k :*: repUpdateMember a right_k


    Uh oh - that doesn't compile! GHC is telling us that we have duplicate instance declarations - but why?
    After all, the two instances clearly differ in their *instance constraints* (i.e., the parts left of the
    '=>') - can't GHC see that?

  - Unfortunately, it can't. We've run into a well-known limitation of GHC's type class instance resolution
    process: the compiler only looks at the *instance head*, i.e., what's to the right of '=>', and it completely
    ignores what's to the left. And in that respect, our two instamce definitions are indeed identical - therefore,
    GHC cannot pick either above the other, and it throws an error.



Avoiding duplicate instances
============================
Luckily, since the limitation we've run into is so well known and so frequently encountered, several workarounds
have been devised for it.


Add an extra parameter to the type class
----------------------------------------
- One trick we could use is described here:

      https://hengchu.github.io/posts/2018-05-09-type-lists-and-type-classes.lhs.html

  The basic idea is that we add to UniqueMemberOfRep an extra type parameter of kind Bool:

    class UniqueMemberOfRep a rep (isMemberOfLeft :: Bool)
      [methods same as before]

  Its value is determined like this:
  - If 'rep' is constructed with :*: , then isMemberOfLeft is true if 'a' is a member of the left-hand operand
    of :*: and false if it's a member of the right-hand operand.
  - In all other cases, isMemberOfLeft is false.

  Now recall that the difference between the two UniqueMemberOfRep instances for ':*:' is the __origin__ of 'a':
  is it a member of the left-hand operand or the right-hand operand? In our failed attempt earlier, this distinction
  was only expressed in the instance constraints - but now it's captured by __a parameter of the type class__.
  Therefore, the two instance heads will be different, and GHC will be happy with our code. 


- The full implementation of this workaround would require the following:

  - Define a type family for computing the value of isMemberOfLeft:

      type family a `IsMemberOfLeft` rep where
        a `IsMemberOfLeft` (left :*: right) = a `IsMemberOfRep` left
        _ `IsMemberOfLeft` _ = 'False


  - Adjust the instance definitions for Rec0, M1 and ':*:' :

      instance UniqueMemberOfRep a (Rec0 a) 'False where
        ...

      instance (UniqueMemberOfRep a rep (a `IsMemberOfLeft` rep)) => UniqueMemberOfRep a (M1 ... rep) 'False where
        ...

      instance (UniqueMemberOfRep a rep1 (IsMemberOfLeft a rep1), a `NotMemberOfRep` rep2) =>
        UniqueMemberOfRep a (rep1 :*: rep2) 'True where
          ...

      instance (a `NotMemberOfRep` rep1, UniqueMemberOfRep a rep2 (IsMemberOfLeft a rep2)) =>
        UniqueMemberOfRep a (rep1 :*: rep2) 'False where
          ...



Without an extra parameter
--------------------------
- The trick above does work, but it feels clumsy: 'isMemberOfLeft' is relevant only when we're dealing with :*:,
  and yet we have to carry it around in all other cases as well. E.g., consider the instance for Rec0:
  'isMemberOfLeft' is actually meaningless there because Rec0 is a unary type constructor, i.e., there's no "left"
  or "right" to speak of. And the same reasoning applies to the instance for M1 as well.

  In other words, since the problem (duplicate instances) stems from :*:, it would be a lot nicer if the workaround
  only affected :*: and left everything else (i.e., the Rec0 and M1 instances) unchanged - but can we achieve that?


- Let's take a step back: the duplicate instances problem was able to come about only because there were two
  instance definitions for :*: to begin with. If we could take care of :*: with __one__ instance definition, then
  we'd nip the problem in the bud.

  But can we actually do that? After all, when dealing with :*: , we have to take care of __two__ cases: one where
  'a' is a member of the left-hand operand of :*:, and one where it's a member of the right-hand operand. How could
  a single instance definition handle both?

  Let's use a common problem-solving trick and assume that *we have already managed to define* the single instance
  in question:
  
    instance (...constraints...) => a `UniqueMemberOfRep` (left :*: right) where
      ...
  
  What would the method definitions look like in that instance?


- Let's consider repExtractMember first: what does it have to do?
  - If the (a `UniqueMemberOfRep` left) instance exists, it has to call repExtractMember on 'left'.
  - If the (a `UniqueMemberOfRep` right) instance exists, it has to call repExtractMember on 'right'.


- Similarly for repUpdateMember:
  - If the (a `UniqueMemberOfRep` left) instance exists, it has to:
    - Call 'repUpdateMember a' on 'left'; let 'newLeft' denote the result.
    - Replace the 'left' argument to :*: with 'newLeft'.

  - If the (a `UniqueMemberOfRep` right) instance exists, it has to:
    - Call 'repUpdateMember a' on 'right'; let 'newRight' denote the result.
    - Replace the 'right' argument to :*: with 'newRight'.


- If you squint at the descriptions above for long enough, you'll realize that they can be simplified to __one__
  case per method by introducing the notion of *selection*:

  - Let 'selected' denote:
    - 'left' if the (a `UniqueMemberOfRep` left) instance exists.
    - 'right' if the (a `UniqueMemberOfRep` right) instance exists.

  - Then repExtractMember simply does this:
    - Call repExtractMember on 'selected'.

  - Similarly for repUpdateMember:
    - Call 'repUpdateMember a' on 'selected'; let 'newSelected' denote the result.
    - Replace with 'newSelected' the argument of :*: that corresponds to 'selected'.


- That looks promising - if we have to handle only one case per method, we should be able to do that with
  __one__ instance definition. But first, let's capture with a type class the selection-related notions we've
  just introduced; note that we use a type-level Boolean parameter to express whether we want to select 'left'
  or 'right': 
-} 

class Select (selectLeft :: Bool) left right where

  type Selected selectLeft left right :: Type -> Type

  selected :: (left :*: right) k -> Selected selectLeft left right k

  replaceSelected :: Selected selectLeft left right k -> (left :*: right) k -> (left :*: right) k


instance Select 'True left right where
  type Selected 'True left right = left
  selected (left_k :*: _) = left_k
  replaceSelected left_k (_ :*: right_k) = left_k :*: right_k

  
instance Select 'False left right where
  type Selected 'False left right = right
  selected (_ :*: right_k) = right_k
  replaceSelected right_k (left_k :*: _) = left_k :*: right_k

{-

- Using Select and following the one-case-per-method descriptions above, the methods in the single instance
  definition for :*: would look something like this:

    instance (
        Select [what comes here?] left right
      , ...other constraints..
      )
      =>
      a `UniqueMemberOfRep` (left :*: right) where

        repExtractMember = selected .> repExtractMember

        repUpdateMember a = dup (selected .> repUpdateMember a .> replaceSelected)


  What should the type-level Boolean argument to Select be? Recall that 'selected' should be 'left' if 'a' comes
  from 'left', and 'right' if 'a' comes from 'right'; so, the 'selectLeft' argument to Select should be true in
  the former case and false in the latter. And actually, we can compute that: that's exactly the result of
  a `IsMemberOfRep` left !


- Due to the rules governing what can and cannot occur in instance constraints, we have to express that in the
  following way:

    instance (
        a `IsMemberOfRep` left ~ selectLeft
      , Select selectLeft left right
      , ...other constraints..
      )
      =>
      a `UniqueMemberOfRep` (left :*: right) where
        ...

  Notice also that

    repExtractMember = selected .> repExtractMember

  is not quite correct yet because we need to pass in to 'selected' the 'selectLeft' type-level argument:

    repExtractMember = selected @selectLeft .> repExtractMember

  And similarly for the other method:

    repUpdateMember a = dup (selected @selectLeft .> repUpdateMember a .> replaceSelected @selectLeft)


- Believe it or not, we're almost done - our last task is to specify the "...other..." instance constraints.
  Recall our failed attempt from earlier:

    instance (a `UniqueMemberOfRep` left, a `NotMemberOfRep` right) => a `UniqueMemberOfRep` (left :*: right) where
      ...
    instance (a `NotMemberOfRep` left, a `UniqueMemberOfRep` right) => a `UniqueMemberOfRep` (left :*: right) where
      ...

  Using Select, we can unify those two constraint pairs like this:

    (a `UniqueMemberOfRep` Selected selectLeft left right, a `NotMemberOfRep` Selected (Not selectLeft) left right)



- Now, at last, we can finally write the single instance definition that takes care of the :*: type constructor:
-}

instance (
    a `IsMemberOfRep` left ~ selectLeft
  , Select selectLeft left right 
  , a `UniqueMemberOfRep` Selected selectLeft left right 
  , a `NotMemberOfRep` Selected (Not selectLeft) left right 
  )
  =>
  a `UniqueMemberOfRep` (left :*: right) where

    repExtractMember = selected @selectLeft .> repExtractMember

    repUpdateMember a = dup $ selected @selectLeft .> repUpdateMember a .> replaceSelected @selectLeft

{-
- And with that, believe it or not, we're done! See Main.hs for a few simple examples that show that liftMemberTrans
  behaves exactly as we imagined it should.
-}
