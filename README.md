What's this?
============
Just a proof-of-concept exercise in using GHC's [datatype-generic programming](https://downloads.haskell.org/ghc/latest/docs/users_guide/exts/generics.html) facilities for *lifting a transformation* from a data member to the entire data type.



Why did I write it?
===================
The use case that led me to this exercise was something like the following:

- Imagine that we have a (record) type that represents some sort of state:

  ```haskell
  data S = S{ ...fields representing the state... }
  ```

- Ths state can be changed by a number of *actions* represented by a type class:

  ```haskell
  class Actions s where
    action1 :: ActionData1 -> s -> s
    action2 :: ActionData2 -> s -> s
    ...
    actionN :: ActionDataN -> s -> s

  instance Actions S where
    action1 ad s = ...
    action2 ad s = ...
    ...
    actionN ad s = ...
  ```

- Later on it turns out that, in addition to `S`, we also need to deal with an *extended* state that requires us to keep track of another field in addition to those already in `S`. Since we're lazy, we decide to model the extended state like this:

  ```haskell
  data ES = ES{ s :: S, newField :: ... }
  ```

- While writing an `Actions` instance for `ES`, we realize that quite a few actions don't actually affect `newField` at all: they operate entirely on the `S` "data member" (to borrow some C++ terminology) of `ES`. If `actionK1, actionK2, ..., actionKp` are such actions, then we can implement them for `ES` like this (using `RecordWildCards`):

  ```haskell
  instance Actions ES where
    ...
    actionK1 ad es@ES{..} = es{ s = actionK1 ad s }
    ...
    actionK2 ad es@ES{..} = es{ s = actionK2 ad s }
    ...
    actionKp ad es@ES{..} = es{ s = actionKp ad s }
    ...
  ```

- But that feels unsatisfactory: it's too verbose. The syntactic noise from the record update machinery obscures the
  essence of what we're doing here:
  - Take `actionKi ad`, which is a *transformation* on S, i.e., it's an `S -> S` function.
  - *Lift* that to a transformation on `ES` by applying the original transformation to the `S` "data member"
    and leaving the rest of `ES` alone.

  The code would reflect our intent much better if we could write something like this:

    ```haskell
    instance Actions ES where
      ...
      actionK1 ad = liftMemberTrans (actionK1 ad)
      ...
      actionK2 ad = liftMemberTrans (actionK2 ad)
      ...
      actionKp ad = liftMemberTrans (actionKp ad)
      ...
    ```


- So, we want to express the following in Haskell: if type `b` has a *data member* of type `a`, and we have an `f :: a -> a` function, then we want to *lift* `f` to a `b -> b` function by applying it to the `a` within `b` and leaving the rest of `b` alone.

  In other words, we want to define:

  ```haskell
  liftMemberTrans :: (a `DataMemberOf` b) => (a -> a) -> (b -> b)
  ```

- But how can we create the required `DataMemberOf` type class instances? E.g., if we have

  ```haskell
  data S = S Int Char
  ```

  then we must ensure that there are instances for 

  ```haskell
  Int `DataMemberOf` S
  ```  
  
  and

  ```haskell
  Char `DataMemberOf` S
  ```    
  
  but how can we do that in a *generic* way that works regardless of the number of fields within `S` and their concrete types? 
  
  Well, unsurprisingly :) this is exactly where datatype-*generic* programming comes in handy, as we'll see soon.



Structure
=========
- [`Trans`](src/Trans.hs) contains the implementation of `liftMemberTrans`, with ample explanation.
- [`Main`](app/Main.hs) just illustrates the usage with a few simple examples.



Disclaimer
==========
- I'm certain that what I did here had already been done before (most likely in `lens` or `optics`). But I redid it anyway, because it was much more fun than just looking something up on Hackage :)

- Also, working through this exercise helped me learn a great deal about datatype-generic programming in general and GHC's `Generic` type class in particular - that's valuable knowledge that I wouldn't have gained if I had simply used the existing functionality in `lens`, for example.
