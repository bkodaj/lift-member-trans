{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Trans( liftMemberTrans )

import GHC.Generics ( Generic )
import Data.Text( Text )
import Numeric.Natural( Natural )


main :: IO ()
main = testLiftTrans


data A = A Text Int Natural Double
  deriving (Generic, Show)    


data B unit id = B id Text Natural unit
  deriving (Generic, Show)


data C = C Text Int Int
  deriving (Generic, Show)  


f :: Text -> Text
f _ = "hello"

g :: Int -> Int
g = ( + 1)

h :: Natural -> Natural
h = ( + 15)

i :: (Num a) => a -> a
i = ( * 2)  


testLiftTrans :: IO ()
testLiftTrans = do
  let a = A "a" 1 100 3.14
  print $ liftMemberTrans f . liftMemberTrans g . liftMemberTrans h . liftMemberTrans @Double i $ a

  let b = B (2 :: Int) "b" 0 (100 :: Integer)
  print $ liftMemberTrans f . liftMemberTrans g . liftMemberTrans h . liftMemberTrans @Integer i $ b

  let c = C "c" 1 2
  print $ liftMemberTrans f c
  -- print $ liftMemberTrans g c  -- Doesn't compile
